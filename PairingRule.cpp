﻿#include"CsvParser.h"
#include"DutyController.h"
#include "PairController.h"
int main()
{
    string flieName = "po_input.csv";

    CsvParser parser = CsvParser();
    DutyController dutyCtr = DutyController();
    NodeController nodeCtr = NodeController();
    PairController pairCtr = PairController();

    parser.readCsv(flieName, nodeCtr);

    nodeCtr.initStartEndNode();
    nodeCtr.sortNodes();
    nodeCtr.linkNodes();

    dutyCtr.findDuty(nodeCtr);
    dutyCtr.printInfo();

    dutyCtr.initStartEndDuty();
    dutyCtr.sortDutys();
    dutyCtr.linkDuty();

    pairCtr.findPairPool(dutyCtr);

    //pairCtr.printInfo();
}

