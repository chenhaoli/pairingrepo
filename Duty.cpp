#include "Duty.h"

time_t Duty::getFlyTime()
{
    time_t flyTime = 0;
    for (int i = 0; i < this->path.size(); ++i) {
        time_t depTime = this->path[i]->getDepUtcTime();
        time_t arvTime = this->path[i]->getArvUtcTime();

        flyTime += (arvTime - depTime);
    }
    return flyTime;
}

time_t Duty::getDutyTime()
{
    time_t dutyTime = 0;
    int len = this->path.size();
    if (len > 0) {
        time_t startTime = this->path[0]->getDepUtcTime();
        time_t endTime = this->path[len-1]->getArvUtcTime();
        dutyTime = endTime - startTime;
    }
    return dutyTime;
}

void Duty::addNode(Node* newNode)
{
    this->path.push_back(newNode);
}

void Duty::removeLastNode()
{
    this->path.pop_back();
}

//bool Duty::checkDutyFlyTime(time_t MAX_FLY_TIME, Node* newNode)
//{
//    bool res = false;
//    if (this->getFlyTime() + newNode->getNodeFlyTime() <= MAX_FLY_TIME) {
//        res = true;
//    }
//    return res;
//}
bool Duty::checkDutyFlyTime(time_t MAX_FLY_TIME)
{
    bool res = false;
    if (this->getFlyTime() <= MAX_FLY_TIME) {
        res = true;
    }
    return res;
}
//bool Duty::checkDutyTime(time_t MAX_DUTY_TIME, Node* newNode)
//{
//    int len = this->path.size();
//    bool res = false;
//    time_t waitTime =newNode->getDepUtcTime() - this->path[len - 1]->getArvUtcTime();
//    if (this->getDutyTime() + newNode->getNodeFlyTime() +  waitTime <= MAX_DUTY_TIME) {
//        res = true;
//    }
//    return res;
//}
bool Duty::checkDutyTime(time_t MAX_DUTY_TIME)
{
    bool res = false;
    if (this->getDutyTime() <= MAX_DUTY_TIME) {
        res = true;
    }
    return res;
}

bool Duty::checkEndBase(string base)
{
    bool res = false;
    int len = this->path.size();
    if (len > 0) {
        if (this->path[len - 1]->flight.arvArp == base) {
            res = true;
        }
    }
    return res;
}

bool Duty::checkStartBase(string base)
{
    bool res = false;
    int len = this->path.size();
    if (len > 0) {
        if (this->path[0]->flight.arvArp == base) {
            res = true;
        }
    }
    return res;
}

void Duty::addNextDuty(Duty* newDuty)
{
    this->nextDutys.push_back(newDuty);
}

bool Duty::checkDutyConnect(Duty newDuty)
{
    bool res = false;
    time_t depTime = Flight::getTimeT(newDuty.path[0]->flight.schDepDtUtcTm);
    time_t arvTime = Flight::getTimeT(this->path[path.size() - 1]->flight.schDepDtUtcTm);

    if (this->path[path.size() - 1]->flight.arvArp == newDuty.path[0]->flight.depArp &&
        arvTime + MIN_REST_TIME <= depTime &&
        arvTime + MAX_REST_TIME >= depTime) {
        res = true;
    }
    return res;
}
