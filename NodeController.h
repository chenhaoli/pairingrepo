#pragma once
#include "Node.h"
class NodeController
{
public:
	vector<Node*> nodePool;
	Node* vStartNode;
	Node* vEndNode;

	static bool cmpNodes(Node* node1, Node* node2);
	void initStartEndNode();
	//void linkStartEnd();
	void addNode(Node* newNode);
	void linkNodes();
	void sortNodes();
};

