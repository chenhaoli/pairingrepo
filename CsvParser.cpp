#include "CsvParser.h"

void CsvParser::setHeaders(string str)
{
	stringstream ss(str);
	string s;
	while (getline(ss, s, ','))
	{
		headers.push_back(s);
	}
}

void CsvParser::fromCsv(vector<string>& headers, int index, char* value, Flight* ref)
{
	if (headers[index] == "id") { ref->id = atoi(value); }
	else if (headers[index] == "schDepDtUtc") { ref->setDepUtcTm(value); }
	else if (headers[index] == "schArvDtUtc") { ref->setArvUtcTm(value); }
	else if (headers[index] == "depArp") { ref->depArp = (value); }
	else if (headers[index] == "arvArp") { ref->arvArp = (value); }
	//else { cout << "ERROR: invalid data, unknown field '" << headers[index] << "'\n"; }
}

void* CsvParser::createInstance()
{
	return new Flight();
}

void CsvParser::addInstance(void* obj, NodeController& nodeCtr)
{
	Flight* flight = new Flight((Flight*)obj);
	nodeCtr.addNode(new Node(flight));
	//(this->flights).push_back(flight);
}

vector<string>& CsvParser::getDefaultHeaders()
{
	return headers;
}

void CsvParser::readCsv(string flieName, NodeController& nodeCtr)
{
	ifstream ifs(flieName, ios::in);
	string line;
	string headStr = "";
	Flight* obj = new Flight();
	while (getline(ifs, line)) {
		//cout << line.substr(0, 19) << endl;
		if (line.substr(0, 18) == "------Flight(1180)") {
			headStr = line.substr(19, line.length() - 19);
			setHeaders(headStr);
			while (getline(ifs, line)) {
				stringstream ss(line);
				string str;
				for (int i = 0; i < getDefaultHeaders().size(); ++i) {
					getline(ss, str, '^');
					fromCsv(getDefaultHeaders(), i, (char*)str.data(), obj);
				}
				addInstance(obj, nodeCtr);
			}
			break;
		}
	}
	ifs.close();

}
