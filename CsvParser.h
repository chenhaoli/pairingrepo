#pragma once
#include"Flight.h"
#include"NodeController.h"
class CsvParser
{
public:
	vector<string> headers;
	//vector<Flight*> flights;

	void setHeaders(string str);
	void fromCsv(vector<string>& headers, int index, char* value, Flight* obj);
	void* createInstance();
	void addInstance(void* obj, NodeController& nodeCtr);
	vector<string>& getDefaultHeaders();
	void readCsv(string flieName, NodeController& nodeCtr);
	vector<Flight*>& getFlights();
};

