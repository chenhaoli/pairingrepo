#include "Node.h"

Node::Node()
{

}

Node::Node(Flight* newFlight)
{
	this->flight = newFlight;
	nextNodes = {};
}

void Node::addNextNode(Node* newNode)
{
	this->nextNodes.push_back(newNode);
}

bool Node::checkNodeConnect(Node* nextNode)
{
	// 本航班终点==下一航班起点
	// 本航班到达时间 + 准备时间 <= 下一航班起飞时间
	time_t arvTime = this->flight.getTimeT(this->flight.schArvDtUtcTm);
	time_t depTime = nextNode->flight.getTimeT(nextNode->flight.schDepDtUtcTm);

	if (this->flight.arvArp == nextNode->flight.depArp &&
		arvTime + WAIT_MIN < depTime &&
		arvTime + WAIT_MAX > depTime) {
		return true;
	}
	return false;
}

time_t Node::getNodeFlyTime()
{
	return Flight::getTimeT(this->flight.schArvDtUtcTm) - Flight::getTimeT(this->flight.schDepDtUtcTm);
}

time_t Node::getDepUtcTime()
{
	return Flight::getTimeT(this->flight.schDepDtUtcTm);
}

time_t Node::getArvUtcTime()
{
	return Flight::getTimeT(this->flight.schArvDtUtcTm);
}

//bool Node::isValidStartNode()
//{
//	return true;
//}
//
//bool Node::isValidEndNode()
//{
//	return true;
//}
