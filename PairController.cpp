#include "PairController.h"

void PairController::findPair(Duty* currentDuty, Pair& currentPair, DutyController& dutyCtr)
{
	for (int i = 0; i < currentDuty->nextDutys.size(); ++i) {
		if (currentDuty->nextDutys[i] == dutyCtr.vEndDuty) {
			this->pairPool.push_back(currentPair);
			continue;
		}
		currentPair.addDuty(currentDuty->nextDutys[i]);
		if (currentPair.checkPairSize(MAX_DUTY_SIZE)) {
			findPair(currentDuty->nextDutys[i], currentPair, dutyCtr);
		}
		currentPair.removeLastDuty();
	}
}

void PairController::findPairPool(DutyController& dutyCtr)
{
	Pair currentPair;
	this->findPair(dutyCtr.vStartDuty, currentPair, dutyCtr);
}

void PairController::printInfo()
{
	for (int i = 0; i < this->pairPool.size(); ++i) {
		cout << "Pair-" << i << ":";
		for (int j = 0; j < this->pairPool[i].dutys.size(); ++j) {
			cout << "Duty-" << j << ":";
			for (int k = 0; k < this->pairPool[i].dutys[j]->path.size(); ++k) {
				/*cout << this->pairPool[i].dutys[j]->path[k]->flight.schDepDtUtcTm.tm_mon << '-'
					<< this->pairPool[i].dutys[j]->path[k]->flight.schDepDtUtcTm.tm_mday << ' '
					<< this->pairPool[i].dutys[j]->path[k]->flight.schDepDtUtcTm.tm_hour << ':'
					<< this->pairPool[i].dutys[j]->path[k]->flight.schDepDtUtcTm.tm_min << ','
					<< this->pairPool[i].dutys[j]->path[k]->flight.schArvDtUtcTm.tm_mon << '-'
					<< this->pairPool[i].dutys[j]->path[k]->flight.schArvDtUtcTm.tm_mday << ' '
					<< this->pairPool[i].dutys[j]->path[k]->flight.schArvDtUtcTm.tm_hour << ':'
					<< this->pairPool[i].dutys[j]->path[k]->flight.schArvDtUtcTm.tm_min << ' ';*/
				cout << this->pairPool[i].dutys[j]->path[k]->flight.depArp << "==>" << this->pairPool[i].dutys[j]->path[k]->flight.arvArp << ",";
			}
			cout << "||";
		}
		cout << endl;
	}
	cout << this->pairPool.size() << endl;
}
