#pragma once
#include "Node.h"
class Duty
{
public:
	vector<Node*> path;
	vector<Duty*> nextDutys;
	const static int MIN_REST_TIME = 10 * 60 * 60;
	const static int MAX_REST_TIME = 24 * 60 * 60;

	time_t getFlyTime();
	time_t getDutyTime();
	void addNode(Node* newNode);
	void removeLastNode();

	bool checkDutyFlyTime(time_t MAX_FLY_TIME);
	bool checkDutyTime(time_t MAX_DUTY_TIME);

	bool checkEndBase(string base);
	bool checkStartBase(string base);

	void addNextDuty(Duty* newDuty);
	bool checkDutyConnect(Duty newDuty);
};

