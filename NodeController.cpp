#include "NodeController.h"

bool NodeController::cmpNodes(Node* node1, Node* node2)
{
	bool res = false;
	if (node1->getDepUtcTime() < node2->getDepUtcTime()) {
		res = true;
	}
	return res;
}

void NodeController::initStartEndNode()
{
	this->vStartNode = new Node();
	this->vEndNode = new Node();
	for (int i = 0; i < this->nodePool.size(); ++i) {
		this->vStartNode->addNextNode(this->nodePool[i]);
		this->nodePool[i]->addNextNode(this->vEndNode);
	}
}

//void NodeController::linkStartEnd()
//{
//	for (int i = 0; i < this->nodePool.size(); ++i) {
//		this->vStartNode->addNextNode(this->nodePool[i]);
//		this->nodePool[i]->addNextNode(this->vEndNode);
//	}
//}

void NodeController::addNode(Node* newNode)
{
	this->nodePool.push_back(newNode);
}

void NodeController::linkNodes()
{
	//After SortNodes
	for (int i = 0; i < this->nodePool.size(); ++i) {
		for (int j = i + 1; j < this->nodePool.size(); ++j) {
			if (this->nodePool[i]->checkNodeConnect(this->nodePool[j])) {
				this->nodePool[i]->addNextNode(this->nodePool[j]);
			}
		}
	}
}

void NodeController::sortNodes()
{
	sort(this->nodePool.begin(), this->nodePool.end(), NodeController::cmpNodes);
}
