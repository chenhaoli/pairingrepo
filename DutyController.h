#pragma once
#include"Duty.h"
#include"NodeController.h"
class DutyController
{
public:
	vector<Duty> dutyPool;
	const static time_t MAX_FLY_TIME = 8 * 60 * 60;
	const static time_t MAX_DUTY_TIME = 14 * 60 * 60;
	string BASE = "PEK";
	Duty* vStartDuty;
	Duty* vEndDuty;

	void findPath(Node* currentNode, Duty& currentDuty, NodeController& nodeCtr);
	void findDuty(NodeController& nodeCtr);

	static bool cmpDuty(Duty duty1, Duty duty2);
	void initStartEndDuty();
	void linkDuty();
	void sortDutys();

	void printInfo();
};

