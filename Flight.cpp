#include "Flight.h"

Flight::Flight()
{

}

Flight::Flight(Flight* f)
{
	this->id = f->id;
	this->schDepDtUtcTm = f->schDepDtUtcTm;
	this->schArvDtUtcTm = f->schArvDtUtcTm;
	this->depArp = f->depArp;
	this->arvArp = f->arvArp;
}

string Flight::getData(tm& structTime)
{
	string dateStr = to_string(structTime.tm_year + 1900) + "-" + to_string(structTime.tm_mon + 1) + "-" + to_string(structTime.tm_mday);
	return dateStr;
}

time_t Flight::getTimeT(tm& structTime)
{
	return  mktime(&structTime);
}

void Flight::setDepUtcTm(string schDepDtUtcStr)
{
	strToTm(schDepDtUtcStr, schDepDtUtcTm);
}

void Flight::setArvUtcTm(string schArvDtUtcStr)
{
	strToTm(schArvDtUtcStr, schArvDtUtcTm);
}

void Flight::strToTm(string str, tm& structTime)
{
	stringstream ss(str);
	string dateStr, timeStr;
	getline(ss, dateStr, 'T');
	getline(ss, timeStr, 'T');

	stringstream dateSS(dateStr);
	string year, mon, day;
	getline(dateSS, year, '-');
	getline(dateSS, mon, '-');
	getline(dateSS, day, '-');

	stringstream timeSS(timeStr);
	string hour, minute;
	getline(timeSS, hour, ':');
	getline(timeSS, minute, ':');


	structTime.tm_year = atoi((char*)year.data()) - 1900;
	structTime.tm_mon = atoi((char*)mon.data()) - 1;
	structTime.tm_mday = atoi((char*)day.data());

	structTime.tm_hour = atoi((char*)hour.data());
	structTime.tm_min = atoi((char*)minute.data());
	structTime.tm_sec = 0;
}
