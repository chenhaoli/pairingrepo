#include "DutyController.h"

void DutyController::findPath(Node* currentNode, Duty& currentDuty, NodeController& nodeCtr)
{
	for (int i = 0; i < currentNode->nextNodes.size(); ++i) {
		if (currentNode->nextNodes[i] == nodeCtr.vEndNode) {
				this->dutyPool.push_back(currentDuty);
				continue;
		}
		currentDuty.addNode(currentNode->nextNodes[i]);
		if (currentDuty.checkDutyTime(MAX_DUTY_TIME) &&
			currentDuty.checkDutyFlyTime(MAX_FLY_TIME)
			) {
			findPath(currentNode->nextNodes[i], currentDuty, nodeCtr);
		}
		currentDuty.removeLastNode();
	}
}

void DutyController::findDuty(NodeController& nodeCtr)
{
	Duty currentDuty;
	this->findPath(nodeCtr.vStartNode, currentDuty, nodeCtr);
}

bool DutyController::cmpDuty(Duty duty1, Duty duty2)
{
	bool res = false;
	if (duty1.path[0]->getDepUtcTime() < duty2.path[0]->getDepUtcTime()) {
		res = true;
	}
	return res;
}

void DutyController::initStartEndDuty()
{
	this->vStartDuty = new Duty();
	this->vEndDuty = new Duty();
	for (int i = 0; i < this->dutyPool.size(); ++i) {
		if (dutyPool[i].checkStartBase(this->BASE)) {
			this->vStartDuty->addNextDuty(&(this->dutyPool[i]));
		}
		if (dutyPool[i].checkEndBase(this->BASE)) {
			this->dutyPool[i].addNextDuty(this->vEndDuty);
		}
	}
}

void DutyController::linkDuty()
{
	//After SortDutys
	for (int i = 0; i < this->dutyPool.size(); ++i) {
		for (int j = i + 1; j < this->dutyPool.size(); ++j) {
			if (this->dutyPool[i].checkDutyConnect(this->dutyPool[j])) {
				this->dutyPool[i].addNextDuty(&(this->dutyPool[j]));
			}
		}
	}
}

void DutyController::sortDutys()
{
	sort(this->dutyPool.begin(), this->dutyPool.end(), DutyController::cmpDuty);
}

void DutyController::printInfo()
{
	for (int i = 0; i < this->dutyPool.size(); ++i) {
		for (int j = 0; j < this->dutyPool[i].path.size(); ++j) {
			cout << this->dutyPool[i].path[j]->flight.schDepDtUtcTm.tm_mon << '-'
				<< this->dutyPool[i].path[j]->flight.schDepDtUtcTm.tm_mday << ' '
				<< this->dutyPool[i].path[j]->flight.schDepDtUtcTm.tm_hour << ':'
				<< this->dutyPool[i].path[j]->flight.schDepDtUtcTm.tm_min << ','
				<< this->dutyPool[i].path[j]->flight.schArvDtUtcTm.tm_mon << '-'
				<< this->dutyPool[i].path[j]->flight.schArvDtUtcTm.tm_mday << ' '
				<< this->dutyPool[i].path[j]->flight.schArvDtUtcTm.tm_hour << ':'
				<< this->dutyPool[i].path[j]->flight.schArvDtUtcTm.tm_min << ' ';
			cout << this->dutyPool[i].path[j]->flight.depArp << "==>" << this->dutyPool[i].path[j]->flight.arvArp << "||";
		}
		cout << endl;
	}
	cout << this->dutyPool.size() << endl;
}
