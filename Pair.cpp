#include "Pair.h"

void Pair::addDuty(Duty* newDuty)
{
	this->dutys.push_back(newDuty);
}

void Pair::removeLastDuty()
{
	this->dutys.pop_back();
}

bool Pair::checkPairSize(int MAX_DUTY_SIZE)
{
	bool res = false;
	if (this->dutys.size() <= MAX_DUTY_SIZE) {
		res = true;
	}
	return res;
}
