#pragma once
#include"Duty.h"
class Pair
{
public:
	vector<Duty*> dutys;

	void addDuty(Duty* newDuty);
	void removeLastDuty();
	bool checkPairSize(int MAX_DUTY_SIZE);
};

