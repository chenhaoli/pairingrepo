#pragma once
#include "Pair.h"
#include "DutyController.h"
class PairController
{
public:
	vector<Pair> pairPool;
	const static int MAX_DUTY_SIZE = 4;

	void findPair(Duty* currentDuty, Pair& currentPair, DutyController& dutyCtr);
	void findPairPool(DutyController& dutyCtr);

	void printInfo();
};

