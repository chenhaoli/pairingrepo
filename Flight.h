#pragma once
#include"include.h"

class Flight
{
public:
	int id;
	tm schDepDtUtcTm;
	tm schArvDtUtcTm;
	string depArp;
	string arvArp;
	
	Flight();
	Flight(Flight* f);
	static string getData(tm& structTime);
	static time_t getTimeT(tm& structTime);

	void setDepUtcTm(string schDepDtUtcStr);
	void setArvUtcTm(string schArvDtUtcStr);
	void strToTm(string str, tm& structTime);
};

