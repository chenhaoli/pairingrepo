#pragma once
#include "Flight.h"
class Node
{
public:
	const static time_t WAIT_MIN = 40 * 60;
	const static time_t WAIT_MAX = 3 * 60 * 60;

	Flight flight;
	vector<Node*> nextNodes;

	Node();
	Node(Flight* newFlight);
	void addNextNode(Node* newNode);
	bool checkNodeConnect(Node* nextNode);
	time_t getNodeFlyTime();
	time_t getDepUtcTime();
	time_t getArvUtcTime();
	//bool isValidStartNode();
	//bool isValidEndNode();
};

